import java.util.ArrayList;
import java.util.Collections;

public class Bingo {
	
	private ArrayList<Integer> numerosBingo = new ArrayList<Integer>();
	private ArrayList<Integer> numerosCantados = new ArrayList<Integer>();
    private ArrayList < Player > jugadores = new ArrayList < Player > ();
	private boolean playing = true;
	
	Bingo(int numerosCanton){
		for(int i = 0; i<numerosCanton;i++) {
			this.numerosBingo.add(i);
		}
	}
	public void jugadores(int cantidadJugadores) {
		for(int i = 0; i < cantidadJugadores; i++) {
			jugadores.add(new Player(numerosBingo, i));
		}
	}
	public void jugando() {
		while(playing) {
			Collections.shuffle(numerosBingo);
			numerosCantados.add(numerosBingo.get(0));
			System.out.println("Los numeros cantados son: " + numerosCantados);
			for(Player jugador: jugadores) {
				jugador.numeroObtenido(numerosBingo.get(0));
				if(jugador.isGanador()) {
					this.playing = false;
				}
			}
			numerosBingo.remove(0);
		}
	}
	
}
