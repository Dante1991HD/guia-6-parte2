import java.util.*;
public class Player {
	
	private HashSet <Integer> carton = new HashSet <>();
	private HashSet <Integer> numerosObtenidos = new HashSet <>();
	private int id;
	private boolean ganador = false;
	
	Player(ArrayList <Integer> numerosBingo, int id) {
		this.id = id;
		Collections.shuffle(numerosBingo);
		for(int i = 0; i < 6; i++) {
			carton.add(numerosBingo.get(i));
		}
		System.out.println("Caton jugador " + id + " es: " + carton);
	}
	
	public void numeroObtenido(int numero) {
		if(carton.contains(numero)) {
			numerosObtenidos.add(numero);
			System.out.println("Caton jugador " + id + " es: " + numero);
		}
		if(numerosObtenidos.containsAll(carton)) {
			this.ganador = true;
			System.out.println("El jugador " + id + " es el ganador");
		}
	}

	public boolean isGanador() {
		return ganador;
	}
	
}
